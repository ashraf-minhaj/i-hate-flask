# Simple Flask Application
Practice repo to build, test and deploy contianers using Gitlab Auto DevOps

## pipeline 
The workflow runs on every `feature` or `feature/feature_name` branches. 
![pipeline](docs/feat-pipeline.png)

This workflow is intented for only deployable branches such as `main` or `stage` -
![prod](docs/prod-pipeline.png)

## Server Setup

### Install and Enable Docker
This container was deployed in an Amazon Linux Machine. These commands were run to install, run and enable docker on startup -
```bash
sudo yum update -y
sudo yum install docker -y

sudo systemctl start docker
sudo systemctl enable docker
```

### Generate SSH key for GitLab AutoDevOps
- generate ssh key 
 ```bash
 ssh-keygen -t rsa -b 4096 -C "min@dev.com"
 ```
- directory `autodevops`
- Add the Public Key to authorized_keys 
 ```bash
 cat autodevops.pub >> ~/.ssh/authorized_keys
 ```
- Get and then add the private key to your repository’s secrets 
 ```bash
 cat autodevops
 ```

#### GitLab Variables
- HOST_IP (your server's public IP address)
- HOST_USER (server's user name)
- SSH_PRIVATE_KEY (it is what it is)

#### Compose file on server
> For simplicity and time it just deploys the image tagged with latest.
> But in production you should always ignore using latest tagged image.

Copy this file to your server. Manual sucks, but hey, time is short right?

```yml
services:
  app:
    container_name: app
    image: registry.gitlab.com/ashraf-minhaj/i-hate-flask:latest
    ports:
      - 80:8080
```

I tried a fully automated DevOps project with GitHub Actions too, find it [here](https://github.com/ashraf-minhaj/HushHub-Backend/).

> ashraf-minhaj was here
> [linkedin.com/ashraf-minhaj](https://www.linkedin.com/in/ashraf-minhaj/)